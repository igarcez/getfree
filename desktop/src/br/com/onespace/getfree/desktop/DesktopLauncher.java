package br.com.onespace.getfree.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import br.com.onespace.getfree.GetFree;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class DesktopLauncher {
    public static boolean rebuildAtlas = false;
    public static boolean drawDebugLine = true;

	public static void main (String[] arg) {
        if(rebuildAtlas){
            TexturePacker.Settings settings = new TexturePacker.Settings();
            settings.maxWidth = 1024;
            settings.maxHeight = 1024;
            settings.debug = drawDebugLine;
            TexturePacker.process(settings, "../../desktop/assets-raw/images", "../../android/assets/images", "getfree.pack");
            //TexturePacker.process(settings, "../../desktop/assets-raw/images-ui", "../../android/assets/images", "canyonbunny-ui.pack");
        }


		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 480;
        config.height = 800;
		new LwjglApplication(new GetFree(), config);
	}
}
