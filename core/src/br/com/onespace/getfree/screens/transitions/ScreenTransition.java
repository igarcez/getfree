package br.com.onespace.getfree.screens.transitions;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Criado por Ian Garcez - OneSpace em 17/12/14.
 * http://onespace.com.br
 */
public interface ScreenTransition {
    public float getDuration();

    public void render(SpriteBatch batch, Texture currScreen, Texture nextScreen, float alpha);
}
