package br.com.onespace.getfree.screens;

/**
 * Criado por Ian Garcez - OneSpace em 17/12/14.
 * http://onespace.com.br
 */
import br.com.onespace.getfree.screens.transitions.ScreenTransition;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

public class DirectedGame implements ApplicationListener {
    private boolean init;
    private AbstractGameScreen currScreen;
    private AbstractGameScreen nextScreen;
    private FrameBuffer currFbo;
    private FrameBuffer nextFbo;
    private SpriteBatch batch;
    private float t;
    private ScreenTransition screenTransition;

    public void setScreen(AbstractGameScreen screen){
        setScreen(screen, null);
    }

    public void setScreen(AbstractGameScreen screen, ScreenTransition screenTransition){
        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();

        if(!init){
            currFbo = new FrameBuffer(Format.RGBA8888, w, h, false);
            nextFbo = new FrameBuffer(Format.RGBA8888, w, h, false);
            batch = new SpriteBatch();
            init = true;
        }

        // começa uma nova transição
        nextScreen = screen;
        nextScreen.show(); // ativa a nova tela
        nextScreen.resize(w, h);
        nextScreen.render(0); // deixar a tela atualizar uma vez
        if(currScreen != null) currScreen.pause();
        nextScreen.pause();
        Gdx.input.setInputProcessor(null); // desabilita input
        this.screenTransition = screenTransition;
        t = 0;
    }

    @Override
    public void create() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resize(int width, int height) {
        if(currScreen != null) currScreen.resize(width, height);
        if(nextScreen != null) nextScreen.resize(width, height);
    }

    @Override
    public void render() {
        // pega o deltaTime e assegura que ele é no máximo 1/60 de segundo
        float deltaTime = Math.min(Gdx.graphics.getDeltaTime(), 1.0f / 60.0f);

        if(nextScreen == null){
            // nenhuma mudança acontecendo
            if(currScreen != null) currScreen.render(deltaTime);
        } else {
            // mudanca ocorrendo
            float duration = 0;
            if(screenTransition != null)
                duration = screenTransition.getDuration();

            // atualiza progress da mudança ativa
            t = Math.min(t + deltaTime, duration);
            if(screenTransition == null || t >= duration){
                // nenhum efeito de transicao definido ou transicao já acabou
                if(currScreen != null) currScreen.hide();
                nextScreen.resume();
                // habilita o input para a proxima tela
                Gdx.input.setInputProcessor(nextScreen.getInputProcessor());
                // altera as telas
                currScreen = nextScreen;
                nextScreen = null;
            } else {
                // renderiza as telas para frame buffer
                currFbo.begin();
                if(currScreen != null) currScreen.render(deltaTime);
                currFbo.end();

                nextFbo.begin();
                nextScreen.render(deltaTime);
                nextFbo.end();

                // renderiza o efeito de alteração de tela na tela
                float alpha = t / duration;
                screenTransition.render(batch, currFbo.getColorBufferTexture(), nextFbo.getColorBufferTexture(), alpha);
            }
        }


    }

    @Override
    public void pause() {
        if (currScreen != null) currScreen.pause();

    }

    @Override
    public void resume() {
        if(currScreen != null) currScreen.resume();

    }

    @Override
    public void dispose() {
        if(currScreen != null) currScreen.hide();
        if(nextScreen != null) nextScreen.hide();

        if(init){
            currFbo.dispose();
            currScreen = null;
            nextFbo.dispose();
            nextScreen = null;
            batch.dispose();
            init = false;
        }

    }

}
