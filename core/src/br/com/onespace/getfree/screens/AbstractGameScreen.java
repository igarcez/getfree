package br.com.onespace.getfree.screens;

import br.com.onespace.getfree.game.Assets;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public abstract class AbstractGameScreen implements Screen {
    Game game;

    public AbstractGameScreen (Game game) {
        this.game = game;
    }

    public abstract void render (float deltaTime);

    public abstract void resize (int width, int height);

    public abstract void show ();

    public abstract void hide ();

    public abstract void pause ();

    public abstract InputProcessor getInputProcessor ();

    public void resume () {
        // todo por enquanto isso não é executado
        //Assets.instance.init(new AssetManager());
    }

    public void dispose () {
        Assets.instance.dispose();
    }
}
