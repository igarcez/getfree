package br.com.onespace.getfree.screens;

import br.com.onespace.getfree.game.Assets;
import br.com.onespace.getfree.game.WorldController;
import br.com.onespace.getfree.game.WorldRenderer;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public class GameScreen extends AbstractGameScreen {
    public static final String TAG = GameScreen.class.getName();

    private WorldRenderer worldRenderer;
    private WorldController worldController;
    private boolean paused;

    public GameScreen(Game game) {
        super(game);
    }

    @Override
    public void render(float deltaTime) {
        //TODO precisa não atualizar o mundo se o game estiver pausado
        if(!paused) worldController.update(deltaTime);
        // cor cornflower blue

        //todo pedir ao douglas arte da agua transparente
        // pq dessa forma posso controlar a cor da agua aqui.
        Gdx.gl.glClearColor(0x64 / 255.0f,
                0x95 / 255.0f,
                0xed / 255.0f,
                0xff / 255.0f);
        // limpa a tela
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldRenderer.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Assets.instance.init(new AssetManager());
        worldController = new WorldController(game);
        worldRenderer = new WorldRenderer(worldController);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public InputProcessor getInputProcessor() {
        return null;
    }
}
