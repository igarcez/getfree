package br.com.onespace.getfree.util;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public class Constants {
    public static final String VERSION = "0.0.3";
    public static final float VIEWPORT_WIDTH = 8.0f;
    public static final float VIEWPORT_HEIGHT = 12.0f;
    public static final float VIEWPORT_GUI_WIDTH = 480.0f;
    public static final float VIEWPORT_GUI_HEIGHT = 800.0f;

    public static final String TEXTURE_ATLAS_OBJECT = "images/getfree.pack";

    public static final String LEVEL_TEST = "levels/test1.png";

    // boat
    public static final float BOAT_INICIAL_VELOCITY_X = 0;
    public static final float BOAT_INICIAL_VELOCITY_Y = 2.0f;
    public static final float BOAT_VELOCITY_INC_PER_LEVEL = 1.0f;
    public static final float BOAT_INICIAL_ACCEL_X = 0;
    public static final float BOAT_INICIAL_ACCEL_Y = 1.0f;
    public static final float BOAT_TERMINAL_VELOCITY_X = 10.0f;
    public static final float BOAT_TERMINAL_VELOCITY_Y = 10.0f;
    public static final float RIVER_LEFT = 2.0f;
    public static final float RIVER_RIGHT = 5.0f;
    public static final float JUMP_MAX_DURATION = 1.5f;
    public static final float BOAT_ACCEL_CONTROL_FACTOR = 5f;

}
