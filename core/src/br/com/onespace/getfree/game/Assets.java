package br.com.onespace.getfree.game;

import br.com.onespace.getfree.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

/**
 * Criado por Ian Garcez - OneSpace em ${DATE}.
 * http://onespace.com.br
 */
public class Assets implements Disposable, AssetErrorListener{
    public static final String TAG = Assets.class.getName();
    public static final Assets instance = new Assets();
    private AssetManager assetManager;

    public AssetRiverSide riverSide;
    public AssetBoat boat;
    public AssetStone stone;
    public AssetTreeLog treeLog;
    public AssetFont fonts;

    private Assets(){}

    public void init(AssetManager assetManager){
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);

        assetManager.load(Constants.TEXTURE_ATLAS_OBJECT, TextureAtlas.class);
        assetManager.finishLoading();
        Gdx.app.debug(TAG, "# de assets carregados: " + assetManager.getAssetNames().size);

        for(String a : assetManager.getAssetNames())
            Gdx.app.debug(TAG, "asset: " + a);

        TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECT);

        // for pixel otimization
        for(Texture t: atlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // instanciando os objetos
        riverSide = new AssetRiverSide(atlas);
        boat = new AssetBoat(atlas);
        stone = new AssetStone(atlas);
        treeLog = new AssetTreeLog(atlas);

        // ui
        fonts = new AssetFont();
    }

    @Override
    public void error(@SuppressWarnings("rawtypes") AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(asset.getClass().getSimpleName(), "Couldn't load asset '" + asset + "'", throwable);
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }

    public class AssetRiverSide{
        public final TextureAtlas.AtlasRegion riverside;
        public final TextureAtlas.AtlasRegion riversideBorder;

        public AssetRiverSide(TextureAtlas atlas){
            riverside = atlas.findRegion("riverside");
            riversideBorder = atlas.findRegion("riverside_border");
        }
    }

    public class AssetBoat{
        public final TextureAtlas.AtlasRegion boat;

        public AssetBoat(TextureAtlas atlas){
            boat = atlas.findRegion("boat");
        }
    }

    public class AssetStone {
        public final TextureAtlas.AtlasRegion stone;

        public AssetStone(TextureAtlas atlas) {
            stone = atlas.findRegion("stone");
        }
    }

    public class AssetTreeLog {
        public final TextureAtlas.AtlasRegion treeLog;

        public AssetTreeLog(TextureAtlas atlas) {
            treeLog = atlas.findRegion("tree_log");
        }
    }

    public class AssetFont {
        public final BitmapFont defaultSmall;
        public final BitmapFont defaultNormal;
        public final BitmapFont defaultBig;

        public AssetFont() {
            // unsing native font yet
            defaultSmall = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
            defaultNormal = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
            defaultBig = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);

            // sizes
            defaultSmall.scale(0.75f);
            defaultNormal.scale(1.0f);
            defaultBig.scale(2.0f);

            // linear filtering to smooth it out
            defaultSmall.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            defaultNormal.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            defaultBig.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        }
    }
}
