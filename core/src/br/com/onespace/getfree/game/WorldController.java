package br.com.onespace.getfree.game;

import br.com.onespace.getfree.game.objects.Boat;
import br.com.onespace.getfree.game.objects.RiverSide;
import br.com.onespace.getfree.game.objects.Stone;
import br.com.onespace.getfree.game.objects.TreeLog;
import br.com.onespace.getfree.screens.GameScreen;
import br.com.onespace.getfree.util.CameraHelper;
import br.com.onespace.getfree.util.Constants;
import com.badlogic.gdx.*;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.sun.media.jfxmediaimpl.MediaDisposer;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public class WorldController extends InputAdapter {
    public static final String TAG = WorldController.class.getName();
    private Game game;
    public float distance;

    // for collisions
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();

    private boolean boatChangingSide;
    private float boatDestination;

    //debug
    private float duration = 0;

    public CameraHelper cameraHelper;
    public Level level;

    /**
     * pausa o movimento para debug
     */
    public boolean debugPause;

    public WorldController(Game game) {
        this.game = game;
        init();
    }

    public void init(){
        Gdx.input.setInputProcessor(this);
        cameraHelper = new CameraHelper();
        debugPause = false;
        initLevel();
    }

    public void initLevel(){
        boatChangingSide = false;
        level = new Level(Constants.LEVEL_TEST);
        level.boat.acceleration = new Vector2(Constants.BOAT_INICIAL_ACCEL_X, Constants.BOAT_INICIAL_ACCEL_Y);
        level.boat.velocity = new Vector2(Constants.BOAT_INICIAL_VELOCITY_X, Constants.BOAT_INICIAL_VELOCITY_Y);
        level.boat.terminalVelocity = new Vector2(Constants.BOAT_TERMINAL_VELOCITY_X, Constants.BOAT_TERMINAL_VELOCITY_Y);
        cameraHelper.setTarget(level.boat);
    }

    public void update(float deltaTime){
        distance = level.boat.position.y;
        handleDebugInput(deltaTime);
        handleInputGame();
        handleBoatAccel(deltaTime);
        level.update(deltaTime);
        testCollisions();
        cameraHelper.update(deltaTime);
    }

    public void handleDebugInput(float deltaTime){
        // garante que as funções de debug só estão disponíveis no pc;
        if(Gdx.app.getType() != Application.ApplicationType.Desktop) return;

        if(!cameraHelper.hasTarget(level.boat)) {
            // controles da camera movimento
            float camMoveSpeed = 5 * deltaTime;
            float camMoveAccelerationFactor = 5;
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) camMoveSpeed *= camMoveAccelerationFactor;
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) moveCamera(-camMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) moveCamera(camMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) moveCamera(0, camMoveSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) moveCamera(0, -camMoveSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.BACKSPACE)) cameraHelper.setPosition(0, 0);
        }

        // controles da camera zoom
        float camZoomSpeed = 1* deltaTime;
        float camZoomSpeedAccelerationFactor = 5;
        if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) camZoomSpeed *= camZoomSpeedAccelerationFactor;
        if(Gdx.input.isKeyPressed(Input.Keys.J)) cameraHelper.addZoom(camZoomSpeed);
        if(Gdx.input.isKeyPressed(Input.Keys.K)) cameraHelper.addZoom(-camZoomSpeed);
        if(Gdx.input.isKeyPressed(Input.Keys.L)) cameraHelper.setZoom(1);

        // controles de debug
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) debugPause = !debugPause;
    }

    private void moveCamera(float x, float y) {
        x += cameraHelper.getPostion().x;
        y += cameraHelper.getPostion().y;
        cameraHelper.setPosition(x, y);
    }

    private void handleInputGame() {
        if(cameraHelper.hasTarget(level.boat)) {
            if(Gdx.app.getType() == Application.ApplicationType.Desktop)
                handleDesktopInput();
            else
                handleMobileInput();

            processChangingSide();
        }
    }

    private void handleDesktopInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
            turnBoatLeft();
        else if(Gdx.input.isKeyJustPressed(Input.Keys.RIGHT))
            turnBoatRight();

        if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
            level.boat.setJumping(true);
        else
            level.boat.setJumping(false);
    }

    private void handleMobileInput(){
        boolean touchOnLeft = Gdx.input.justTouched() && Gdx.input.getX() < Gdx.graphics.getWidth() / 2;
        boolean touchOnRight = Gdx.input.justTouched() && Gdx.input.getX() > Gdx.graphics.getWidth() / 2;

        if(touchOnLeft && boatDestination == Constants.RIVER_LEFT)
            level.boat.setJumping(true);
        else if(touchOnRight && boatDestination == Constants.RIVER_RIGHT)
            level.boat.setJumping(true);
        else
            level.boat.setJumping(false);

        if(touchOnLeft)
            turnBoatLeft();
        else if(touchOnRight)
            turnBoatRight();

    }

    private void turnBoatLeft() {
        boatChangingSide = true;
        boatDestination = Constants.RIVER_LEFT;
        level.boat.velocity.x = -level.boat.velocity.y;
    }

    private void turnBoatRight() {
        boatChangingSide = true;
        boatDestination = Constants.RIVER_RIGHT;
        level.boat.velocity.x = level.boat.velocity.y;
    }

    private void processChangingSide(){
        if(boatChangingSide){
            if (boatDestination == Constants.RIVER_RIGHT){
                if(level.boat.position.x >= boatDestination){
                    level.boat.position.x = boatDestination;
                    level.boat.velocity.x = 0;
                    boatChangingSide = false;
                }
            }

            if(boatDestination == Constants.RIVER_LEFT){
                if(level.boat.position.x <= boatDestination){
                    level.boat.position.x = boatDestination;
                    level.boat.velocity.x = 0;
                    boatChangingSide = false;
                }
            }
        }
    }

    private void setDuration(float number) {
        this.duration = number;
    }

    private void handleBoatAccel(float deltaTime){
        float velLimitPerLevel = (float) level.currentLvNumber / Constants.BOAT_ACCEL_CONTROL_FACTOR;
        float currentVelY = level.boat.velocity.y;

        if(currentVelY > velLimitPerLevel && currentVelY > (int) Constants.BOAT_INICIAL_VELOCITY_Y){
            level.boat.acceleration.y = 0;
            level.boat.velocity.y = currentVelY;
        } else {
            level.boat.acceleration.y = Constants.BOAT_VELOCITY_INC_PER_LEVEL;
        }
    }

    private void testCollisions(){
        // cant collide on air
        if (level.boat.jumpState != Boat.JUMP_STATE.GROUNDED) return;

        r1.set(
                level.boat.position.x, level.boat.position.y,
                level.boat.bounds.width, level.boat.bounds.height
        );

        // collision test boat <-> riverside
        for(RiverSide riversideUnit : level.riverSides){
            r2.set(
                    riversideUnit.position.x, riversideUnit.position.y,
                    riversideUnit.bounds.width, riversideUnit.bounds.height
            );

            if(!r1.overlaps(r2)) continue;
            onCollisionBoatWithRiverside(riversideUnit);
        }

        // collision test boat <-> riverside next
        for(RiverSide riversideUnit : level.nextRiverSides){
            r2.set(
                    riversideUnit.position.x, riversideUnit.position.y,
                    riversideUnit.bounds.width, riversideUnit.bounds.height
            );

            if(!r1.overlaps(r2)) continue;
            onCollisionBoatWithRiverside(riversideUnit);
        }

        // collision test boat <-> stone;
        for(Stone stone : level.stones){
            r2.set(
                    stone.position.x, stone.position.y,
                    stone.bounds.width, stone.bounds.height
            );

            if(!r1.overlaps(r2)) continue;
            onCollisionBoatWithStone(stone);
        }

        // collision test boat <-> stone next;
        for(Stone stone : level.nextStones){
            r2.set(
                    stone.position.x, stone.position.y,
                    stone.bounds.width, stone.bounds.height
            );

            if(!r1.overlaps(r2)) continue;
            onCollisionBoatWithStone(stone);
        }

        // collision test boat <-> treelog
        for(TreeLog treeLog : level.treeLogs) {
            r2.set(
                    treeLog.position.x, treeLog.position.y,
                    treeLog.bounds.width, treeLog.bounds.height
            );

            if(!r1.overlaps(r2)) continue;
            onCollisionBoatWithTreeLog(treeLog);
        }

        // collision test boat <-> treelog next
        for(TreeLog treeLog : level.nextTreeLogs) {
            r2.set(
                    treeLog.position.x, treeLog.position.y,
                    treeLog.bounds.width, treeLog.bounds.height
            );

            if(!r1.overlaps(r2)) continue;
            onCollisionBoatWithTreeLog(treeLog);
        }
    }

    private void onCollisionBoatWithRiverside(RiverSide riverSide){
        //todo here it goes to the game over screen, or retry
        //init();
        //Gdx.app.log(TAG, "collided");
        riverSide.setChangeColor(true);
    }

    private void onCollisionBoatWithStone(Stone stone) {
        stone.setChangeColor(true);
    }

    private void onCollisionBoatWithTreeLog(TreeLog treeLog) {
        treeLog.setChangeColor(true);
    }

    @Override
    public boolean keyUp(int keycode){
        // Resetar o jogo
        if(keycode == Input.Keys.R){
            init();
            Gdx.app.debug(TAG, "Game world was resetted");
        }

        // camera seguindo ou não
        else if(keycode == Input.Keys.ENTER){
            cameraHelper.setTarget(cameraHelper.hasTarget() ? null : level.boat);
            Gdx.app.log(TAG, "Camera follow enabled " + cameraHelper.hasTarget());
        }

        // volta para o menu
        else if(keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK){

        }
        return false;
    }
}
