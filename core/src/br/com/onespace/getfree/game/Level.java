package br.com.onespace.getfree.game;

import br.com.onespace.getfree.game.objects.*;
import br.com.onespace.getfree.util.Constants;
import br.com.onespace.getfree.util.Tools;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public class Level {
    public static final String TAG = Level.class.getName();

    public Pixmap pixmap;

    public enum BLOCK_TYPE{
        EMPTY(0,0,0), //black
        RIVERSIDE(0,255,0), //green
        PLAYER_SPAWNPOINT(255,255,255), // white
        TREE_LOG(135,50,0), // brown
        STONE(130,130,130); // gray
        // ITEM_FEATHER(255,0,255), // purple
        // ITEM_GOLD_COIN(255,255,0), //yellow
        // GOAL(255,0,0); // red

        private int color;

        private BLOCK_TYPE(int r, int g, int b) {
            color = r << 24 | g << 16 | b << 8 | 0xff;
        }

        public boolean sameColor(int color){
            return this.color == color;
        }

        public int getColor(){
            return color;
        }
    }

    // objects
    public Array<RiverSide> riverSides;
    public Array<RiverSide> nextRiverSides;
    public Array<Stone> stones;
    public Array<Stone> nextStones;
    public Array<TreeLog> treeLogs;
    public Array<TreeLog> nextTreeLogs;

    public Boat boat;

    public int lastYPixel;
    public boolean isNextLvLoaded;
    public int currentLvNumber;
    public int oldLv;
    public boolean gameIsOn;


    public Level(String filename) {
        init(filename);
    }

    public void init(String filename){
        riverSides = new Array<RiverSide>();
        nextRiverSides = new Array<RiverSide>();
        stones = new Array<Stone>();
        nextStones = new Array<Stone>();
        treeLogs = new Array<TreeLog>();
        nextTreeLogs = new Array<TreeLog>();
        oldLv = 0;
        gameIsOn = false;

        // load image for this challange
        pixmap = new Pixmap(Gdx.files.internal(filename));

        // load initial level
        loadLevel(pixmap, 0, false);

        // load nextLevel
        loadLevel(pixmap, this.lastYPixel + 1, true);

        isNextLvLoaded = true;

    }

    public void loadLevel(Pixmap pixmap, int verticalDifference, boolean next) {
        // scan the pixels from left top to rigth bottom
        for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
            for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
                AbstractGameObject obj = null;
                int currentPixel = pixmap.getPixel(pixelX, pixelY);
                float offsetWidth = 0;
                // altura aumenta do fundo ao topo
                float baseHeight = pixmap.getHeight() - pixelY;

                // em branco
                if(BLOCK_TYPE.EMPTY.sameColor(currentPixel)){
                    // silence is golden
                }

                // riverside
                else if(BLOCK_TYPE.RIVERSIDE.sameColor(currentPixel)){
                    obj = new RiverSide();
                    //obj.position.set(
                    //       pixelX - ((Constants.VIEWPORT_WIDTH / 2) - obj.dimension.x),
                    //        -pixelY + pixmap.getHeight() + verticalDifference - (Constants.VIEWPORT_HEIGHT / 2) - obj.dimension.y);
                    obj.position.set(
                            pixelX, baseHeight + verticalDifference
                    );
                    if(!next)
                        riverSides.add((RiverSide) obj);
                    else
                        nextRiverSides.add((RiverSide) obj);
                }

                // tronco
                else if(BLOCK_TYPE.TREE_LOG.sameColor(currentPixel)) {
                    obj = new TreeLog();
                    obj.position.set(pixelX, baseHeight + verticalDifference);
                    if(!next)
                        treeLogs.add((TreeLog) obj);
                    else
                        nextTreeLogs.add((TreeLog) obj);
                }

                // stone
                else if(BLOCK_TYPE.STONE.sameColor(currentPixel)){
                    obj = new Stone();
                    obj.position.set(pixelX, baseHeight + verticalDifference);
                    if(!next)
                        stones.add((Stone) obj);
                    else
                        nextStones.add((Stone) obj);
                }

                // player
                else if(BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)){
                    if(!gameIsOn){

                        obj = new Boat();
                        obj.position.set(
                                pixelX, baseHeight
                        );
                        boat = (Boat) obj;
                        gameIsOn = true;
                    }
                }
                // unknown object/pixel color
                else {
                    int r = 0xff & (currentPixel >>> 24); //red color channel
                    int g = 0xff & (currentPixel >>> 16); //green color channel
                    int b = 0xff & (currentPixel >>> 8); //blue color channel
                    int a = 0xff & currentPixel; //alpha channel

                    Gdx.app.error(TAG, "Objeto desconhecido em x<" + pixelX
                                    + "> y<" + pixelY
                                    + ">: r<" + r
                                    + "> g<" + g
                                    + "> b<" + b
                                    + "> a<" + a
                                    + ">"
                    );
                }
            }

            //save last
            lastYPixel = pixelY + verticalDifference;
        }
    }

    public void update(float deltaTime){
        currentLvNumber = MathUtils.floor((boat.position.y + 6.0f) / 24);

        boolean clearPositionIsOkay = (boat.position.y % 24 < 6 && boat.position.y % 24 > 4);

        if(currentLvNumber > oldLv && clearPositionIsOkay){
                cleanOldLevels();
                loadNextLevel();
                oldLv = currentLvNumber;
        }

        for(RiverSide riverSide : riverSides)
            riverSide.update(deltaTime);

        for(RiverSide riverSide : nextRiverSides)
            riverSide.update(deltaTime);

        for(Stone stone : stones)
            stone.update(deltaTime);

        for(Stone stone : nextStones)
            stone.update(deltaTime);

        for(TreeLog treeLog : treeLogs)
            treeLog.update(deltaTime);

        for(TreeLog treeLog : nextTreeLogs)
            treeLog.update(deltaTime);

        boat.update(deltaTime);
    }

    public void render(SpriteBatch batch){
        for(RiverSide riverSide : riverSides)
            riverSide.render(batch);

        for(RiverSide riverSide : nextRiverSides)
            riverSide.render(batch);

        for(Stone stone : stones)
            stone.render(batch);

        for(Stone stone : nextStones)
            stone.render(batch);

        for(TreeLog treeLog : treeLogs)
            treeLog.render(batch);

        for(TreeLog treeLog : nextTreeLogs)
            treeLog.render(batch);

        boat.render(batch);
    }

    public void loadNextLevel(){
        //todo here we gonna load a random lv based in the lv number
        pixmap = new Pixmap(Gdx.files.internal(Constants.LEVEL_TEST));
        changeLevel();

        if(isNextLvLoaded){
            loadLevel(pixmap, lastYPixel + 1, false);
        } else {
            loadLevel(pixmap, lastYPixel + 1, true);
        }

        isNextLvLoaded = !isNextLvLoaded;
    }

    public void cleanOldLevels(){
        if(isNextLvLoaded){
            riverSides.clear();
            stones.clear();
            treeLogs.clear();
        } else {
            nextRiverSides.clear();
            nextStones.clear();
            nextTreeLogs.clear();
        }
    }

    public void changeLevel() {
        int variation = Tools.randInt(1, 5);
        // todo Change this to currentLvNumber / 10
        int mainLevel = (int) Math.floor(currentLvNumber / (Constants.BOAT_ACCEL_CONTROL_FACTOR * 2)) + 1;
        String levelName = "level_" + mainLevel + "_" + variation + ".png";
        try{
            pixmap = new Pixmap(Gdx.files.internal("levels/" + levelName));
            Gdx.app.log(TAG, "Mapa:" + levelName + " carregado");

        } catch(Exception e){
            pixmap = new Pixmap(Gdx.files.internal("levels/level_1_1.png"));
            Gdx.app.log(TAG, "Mapa: " + levelName + " não encontrado");
        }
    }
}
