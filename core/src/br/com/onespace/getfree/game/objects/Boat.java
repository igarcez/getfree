package br.com.onespace.getfree.game.objects;

import br.com.onespace.getfree.game.Assets;
import br.com.onespace.getfree.util.Constants;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Criado por Ian Garcez - OneSpace em 17/09/14.
 * http://onespace.com.br
 */
public class Boat extends AbstractGameObject {

    public enum JUMP_STATE {
        GROUNDED,
        FALLING,
        JUMP_RISING,
        JUMP_FALLING
    }

    private TextureRegion regBoat;
    public float timeJumping;
    public JUMP_STATE jumpState;

    public Boat() {
        init();
    }

    public void init(){
        dimension.set(1, 1);
        terminalVelocity.set(Constants.BOAT_TERMINAL_VELOCITY_X, Constants.BOAT_TERMINAL_VELOCITY_Y);
        origin.set(dimension.x / 2, dimension.y / 2);
        bounds.set(0,0,dimension.x, dimension.y);
        jumpState = JUMP_STATE.JUMP_FALLING;
        timeJumping = 0;
        regBoat = Assets.instance.boat.boat;

    }

    public void setJumping(boolean jumpKeyPressed){
        switch (jumpState) {
            case GROUNDED: //boat on water
                setChangeColor(false);
                if(jumpKeyPressed) {
                    // am.play(Assets.instance.sounds.jump);
                    // start counting jump
                    timeJumping = 0;
                    jumpState = JUMP_STATE.JUMP_RISING;
                }
                break;
            case JUMP_RISING: // getting on air
                setChangeColor(true);
                //if(!jumpKeyPressed){
                //    jumpState = JUMP_STATE.JUMP_FALLING;

                //}
                if(timeJumping >= Constants.JUMP_MAX_DURATION){
                    jumpState = JUMP_STATE.FALLING;
                }
                break;
            case FALLING: // yeah...
            case JUMP_FALLING: //falling after a jump
                //if(jumpKeyPressed){
                    // am.play(Assets.instance.sounds.jumpWithFeather, 1 , MathUtils.random(1.0f, 1.1f));
                //    timeJumping = JUMP_TIME_OFFSET_FLYING;
                //    jumpState = JUMP_STATE.JUMP_RISING;
                //}
                jumpState = JUMP_STATE.GROUNDED;
                break;
        }
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        if (jumpState == JUMP_STATE.JUMP_RISING){
            timeJumping += deltaTime;
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        TextureRegion reg = null;

        // desenha particulas
        // dustParticles.draw(batch);

        // aplica cor personalizada
        // batch.setColor(CharacterSkin.values()[GamePreferences.instance.charSkin].getColor());

        // desenha
        //reg = animation.getKeyFrame(stateTime, true);

        //pulo
        if(changeColor){
            batch.setColor(1,0,0,0.5f);
        }
        reg = regBoat;
        batch.draw(
                reg.getTexture(),
                position.x, position.y,
                origin.x, origin.y,
                dimension.x, dimension.y,
                scale.x, scale.y,
                rotation,
                reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(),
                false, false
        );

        batch.setColor(Color.WHITE);

    }

}
