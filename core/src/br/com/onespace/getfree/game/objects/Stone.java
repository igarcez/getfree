package br.com.onespace.getfree.game.objects;

import br.com.onespace.getfree.game.Assets;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Criado por Ian Garcez - OneSpace em 17/11/14.
 * http://onespace.com.br
 */
public class Stone extends AbstractGameObject {
    private TextureRegion regStone;

    public Stone(){ init(); }

    public void init(){
        dimension.set(1,1);
        bounds.set(0,0,dimension.x, dimension.y);
        regStone = Assets.instance.stone.stone;
    }

    public void render(SpriteBatch batch){
        TextureRegion reg = null;
        reg = regStone;

        if(changeColor){
            batch.setColor(1,0,0,0.5f);
        }
        batch.draw(
                reg.getTexture(),
                position.x, position.y,
                origin.x, origin.y,
                dimension.x, dimension.y,
                scale.x, scale.y,
                rotation,
                reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(),
                false, false
        );

        batch.setColor(Color.WHITE);
    }
}
