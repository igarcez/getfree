package br.com.onespace.getfree.game.objects;

import br.com.onespace.getfree.game.Assets;
import br.com.onespace.getfree.util.Constants;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public class RiverSide extends AbstractGameObject {
    public SIDE side;
    private TextureRegion regRiverside;
    private TextureRegion regRiversideBorder;

    public enum SIDE{
        LEFT,
        RIGHT;
    }

    public RiverSide() {
        init();
    }

    public void init(){
        leftSided();
        dimension.set(1, 1);
        bounds.set(0,0,dimension.x, dimension.y);
        regRiverside = Assets.instance.riverSide.riverside;
        regRiversideBorder = Assets.instance.riverSide.riversideBorder;
    }

    public void leftSided(){
        this.side = SIDE.LEFT;
    }

    public void rightSided(){
        this.side = SIDE.RIGHT;
    }

    @Override
    public void render(SpriteBatch batch) {
        TextureRegion reg = null;
        reg = regRiverside;

        if(changeColor){
            batch.setColor(1,0,0,0.5f);
        }
        batch.draw(
            reg.getTexture(),
            position.x, position.y,
            origin.x, origin.y,
            dimension.x, dimension.y,
            scale.x, scale.y,
            rotation,
            reg.getRegionX(), reg.getRegionY(),
            reg.getRegionWidth(), reg.getRegionHeight(),
            false, false
        );

        batch.setColor(Color.WHITE);
    }
}
