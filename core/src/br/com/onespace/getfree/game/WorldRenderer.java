package br.com.onespace.getfree.game;

import br.com.onespace.getfree.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Disposable;

/**
 * Criado por Ian Garcez - OneSpace em 16/09/14.
 * http://onespace.com.br
 */
public class WorldRenderer implements Disposable {
    private WorldController worldController;
    private OrthographicCamera camera;
    private OrthographicCamera cameraGUI;
    private SpriteBatch batch;

    public WorldRenderer(WorldController worldController){
        this.worldController = worldController;
        init();
    }

    public void init(){
        batch = new SpriteBatch();
        camera = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
        camera.position.set(0,0,0);
        // centraliza com o mapinha
        //worldController.cameraHelper.setPosition(3.0f, 6.0f);
        camera.update();

        cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        cameraGUI.position.set(0,0,0);
        cameraGUI.setToOrtho(true);
        cameraGUI.update();
    }

    public void render(){
        renderWorld(batch);
        renderGui(batch);
    }

    public void renderWorld(SpriteBatch batch){
        worldController.cameraHelper.applyTo(camera);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        worldController.level.render(batch);
        batch.end();

    }

    public void renderGui(SpriteBatch batch) {
        batch.setProjectionMatrix(cameraGUI.combined);
        batch.begin();
        renderGuiScore(batch);
        renderGuiFps(batch);
        renderGuiBoatSpeed(batch);
        batch.end();
    }

    public void renderGuiScore(SpriteBatch batch) {
        float x = cameraGUI.viewportWidth / 2;
        float y = 10;
        int score = (int) worldController.distance;
        BitmapFont scoreFont = Assets.instance.fonts.defaultNormal;
        scoreFont.draw(batch, "" + score, x, y);
    }

    public void renderGuiFps(SpriteBatch batch) {
        float x = cameraGUI.viewportWidth - 105;
        float y = cameraGUI.viewportHeight - 45;
        int fps = Gdx.graphics.getFramesPerSecond();
        BitmapFont fpsFont = Assets.instance.fonts.defaultSmall;
        if (fps >= 45) {
            fpsFont.setColor(0,1,0,1); //verde
        } else if (fps >= 30) {
            fpsFont.setColor(1,1,0,1); // amarelo
        } else {
            fpsFont.setColor(1,0,0,1); // vermelho
        }
        fpsFont.draw(batch, "FPS: " + fps, x, y);
        fpsFont.setColor(1,1,1,1);
    }

    public void renderGuiBoatSpeed(SpriteBatch batch){
        float x = 30;
        float y = cameraGUI.viewportHeight -45;
        float boatSpeed = worldController.level.boat.velocity.y;
        BitmapFont speedFont = Assets.instance.fonts.defaultSmall;
        speedFont.draw(batch, "m/s: " + boatSpeed, x,y);
    }

    public void resize(int width, int height){
        camera.viewportWidth = (Constants.VIEWPORT_HEIGHT/ height) * width;
        camera.update();

        cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
        cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / (float) height) * (float) width;
        cameraGUI.position.set(cameraGUI.viewportWidth / 2, cameraGUI.viewportHeight / 2, 0);
        cameraGUI.update();
    }


    @Override
    public void dispose() {
        batch.dispose();
    }
}
