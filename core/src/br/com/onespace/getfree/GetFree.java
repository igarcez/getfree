package br.com.onespace.getfree;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import br.com.onespace.getfree.screens.GameScreen;
import com.badlogic.gdx.Gdx;


public class GetFree extends Game {

    @Override
    public void create(){
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        setScreen(new GameScreen(this));
    }
}
